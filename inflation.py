"""
This Inflation application retreives inflation data and prints them out
"""
__author__ = "Peter Wachira (peter.wchr@gmail.com)"
__version__ = "1.0.0"

import datetime
import urllib2
from operator import itemgetter
from bs4 import BeautifulSoup


class Inflation(object):

    """
    The Inflation class uses the url to the inflation webpage,
    the description of the historical inflation page under it
    and the title of the page with the inflation data.
    It has methods for retreiving and printing inflation data.
    It uses the BeautifulSoup library to wrangle html:
    (https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
    """

    def __init__(self, home_url):
        self.home_page = BeautifulSoup(urllib2.urlopen(
            home_url).read().decode('utf-8'), 'html.parser')
        self.min_date = None
        self.max_date = None
        self.sorted_infl = []

    # Only print 1st occurrence of the paragraph containg key term.
    def get_desc(self, short_desc):
        """
        Return 1st paragraph that contains the search term.
        """
        return ([p for p in self.home_page.find_all(
            'p')if short_desc in str(p)][0].text)

    # return 1st link that has the title
    def get_url_from_title(self, infl_title):
        """
        Return url of the inlfation page
        """
        return [a for a in self.home_page.find_all(
            'a') if infl_title in str(a.get('title'))][0].get('href')

    # Parse rown from  table. 1st table found in the page is used
    @classmethod
    def get_rows(cls, infl_url):
        """
        Return table rows from the url that has the table
        """
        table = BeautifulSoup(urllib2.urlopen(
            infl_url).read().decode('utf-8'), 'html.parser').table
        rows_all = table.tbody.find_all('tr')
    # Drop 1st row (headers)
        return [r for i, r in enumerate(rows_all) if i > 0]

    # Parse list of inflation and date ranges into instance variables
    def parse_inflation(self, rows):
        """
        Parses dates and inflation and save them in instance varables.
        """
        infl_list = []
        date_list = []
        for row in rows:
            year = int(row.th.text)
            for i, td_text in enumerate(row.find_all('td')):
                # Ignore last field (Average Inflation) and blank cells
                # in date determination
                if i > 11 or not td_text.get_text(strip=True):
                    continue
                infl_dt = datetime.date(year, i + 1, 1)
                date_list.append(infl_dt)
            # Select last field (Average Inflation)
            infl = row.select('td')[12].get_text(strip=True)
            # Some rows are missing inflation values
            if infl:
                infl_list.append((year, float(infl)))
        # Sort inflation descending, store results in instance variable
        self.sorted_infl = sorted(infl_list, key=itemgetter(1), reverse=True)
        self.min_date = min(date_list).strftime('%b %Y')
        self.max_date = max(date_list).strftime('%b %Y')


def main():
    """
    Main entry point
    """
    home_url = 'http://www.usinflationcalculator.com/inflation/'
    short_desc = 'annual inflation rates'
    infl_title = 'Historical Inflation Rates: 1914-2008'
    inflation = Inflation(home_url)
    infl_url = inflation.get_url_from_title(infl_title)
    rows = inflation.get_rows(infl_url)
    inflation.parse_inflation(rows)
    print inflation.get_desc(short_desc)
    print 'This page contains data from {0} to {1}'.format(
        inflation.min_date, inflation.max_date)
    print '|{:^6}|{:^10}|'.format('Year', 'Inflation')
    print '_'*19
    for infl in inflation.sorted_infl:
        print '|{:6}|{:10.1f}|'.format(infl[0], infl[1])


if __name__ == '__main__':
    main()
