--using mysql
create database inflation;
create table inflation.history (year date, avg_inflation float);

-- assuming the data is saved to a csv file and in the mysql load directory

load data infile 'test.txt' into table inflation.history;
-- average inflation numbers for each year, sorted by inflation descending.
select year, avg_inflation from inflation.history order by avg_inflation desc;
